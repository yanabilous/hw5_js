function createNewUser() {

    let newUser = {
        firstName: "",
        lastName: "",
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName: function (inputValue) {
            return Object.defineProperty(this, "firstName", {value: inputValue, writable: false});
        },
        setLastName: function (value) {
            return Object.defineProperty(this, "lastName", {value, writable: false});
        },
    };
    newUser.setFirstName(prompt("enter your name!"));
    newUser.setLastName(prompt("enter your last name!"));

    return newUser;
}

let user1 = createNewUser();
let loginOfUser1 = user1.getLogin();
console.log(loginOfUser1);


